//
// Generated By:JAX-WS RI IBM 2.2.1-11/28/2011 08:28 AM(foreman)- (JAXB RI IBM 2.2.3-11/28/2011 06:21 AM(foreman)-)
//


package za.co.teba.bme.service.jaxws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "getPersonResponse", namespace = "http://service.bme.teba.co.za/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getPersonResponse", namespace = "http://service.bme.teba.co.za/")
public class GetPersonResponse {

    @XmlElement(name = "return", namespace = "")
    private za.co.teba.bme.entities.Person _return;

    /**
     * 
     * @return
     *     returns Person
     */
    public za.co.teba.bme.entities.Person getReturn() {
        return this._return;
    }

    /**
     * 
     * @param _return
     *     the value for the _return property
     */
    public void setReturn(za.co.teba.bme.entities.Person _return) {
        this._return = _return;
    }

}
