package za.co.teba.bme.dto;

import java.util.Date;

public class MedicalExamination {
	
	private String isThisFirstExamination;
	private Date datePreviousExamination;
	private String centrePreviousExamination;
	private String previousCertification;
	private Date dateTodayExamination;
	private String whereExaminedToday;
	private String centreTodayExamination;
	
	
	public String getIsThisFirstExamination() {
		return isThisFirstExamination;
	}
	public void setIsThisFirstExamination(String isThisFirstExamination) {
		this.isThisFirstExamination = isThisFirstExamination;
	}
	public Date getDatePreviousExamination() {
		return datePreviousExamination;
	}
	public void setDatePreviousExamination(Date datePreviousExamination) {
		this.datePreviousExamination = datePreviousExamination;
	}
	public String getCentrePreviousExamination() {
		return centrePreviousExamination;
	}
	public void setCentrePreviousExamination(String centrePreviousExamination) {
		this.centrePreviousExamination = centrePreviousExamination;
	}
	public String getPreviousCertification() {
		return previousCertification;
	}
	public void setPreviousCertification(String previousCertification) {
		this.previousCertification = previousCertification;
	}
	public Date getDateTodayExamination() {
		return dateTodayExamination;
	}
	public void setDateTodayExamination(Date dateTodayExamination) {
		this.dateTodayExamination = dateTodayExamination;
	}
	public String getWhereExaminedToday() {
		return whereExaminedToday;
	}
	public void setWhereExaminedToday(String whereExaminedToday) {
		this.whereExaminedToday = whereExaminedToday;
	}
	public String getCentreTodayExamination() {
		return centreTodayExamination;
	}
	public void setCentreTodayExamination(String centreTodayExamination) {
		this.centreTodayExamination = centreTodayExamination;
	}
	

}
