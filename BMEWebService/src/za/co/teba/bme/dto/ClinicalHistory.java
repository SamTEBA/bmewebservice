package za.co.teba.bme.dto;


public class ClinicalHistory {
	
	private String nameOfDoctor;
	private String addressOfDoctor;
	private String hospital;
	private String familyHistory;
	private String previousIllness;
	private String historyOfPresentIllness;
	public String getNameOfDoctor() {
		return nameOfDoctor;
	}
	public void setNameOfDoctor(String nameOfDoctor) {
		this.nameOfDoctor = nameOfDoctor;
	}
	public String getAddressOfDoctor() {
		return addressOfDoctor;
	}
	public void setAddressOfDoctor(String addressOfDoctor) {
		this.addressOfDoctor = addressOfDoctor;
	}
	public String getHospital() {
		return hospital;
	}
	public void setHospital(String hospital) {
		this.hospital = hospital;
	}
	public String getFamilyHistory() {
		return familyHistory;
	}
	public void setFamilyHistory(String familyHistory) {
		this.familyHistory = familyHistory;
	}
	public String getPreviousIllness() {
		return previousIllness;
	}
	public void setPreviousIllness(String previousIllness) {
		this.previousIllness = previousIllness;
	}
	public String getHistoryOfPresentIllness() {
		return historyOfPresentIllness;
	}
	public void setHistoryOfPresentIllness(String historyOfPresentIllness) {
		this.historyOfPresentIllness = historyOfPresentIllness;
	}

	
}
