package za.co.teba.bme.dto;


public class BenefitMedicalExamination {
	
private PersonalDetails personalDetails;
private ClinicalHistory clinicalHistory;
private MedicalExaminationDetails medicalExaminationDetails;
private TransportRemuneration transportRemuneration;
private MedicalExamination medicalExamination;

public MedicalExamination getMedicalExamination() {
	return medicalExamination;
}
public void setMedicalExamination(MedicalExamination medicalExamination) {
	this.medicalExamination = medicalExamination;
}
public PersonalDetails getPersonalDetails() {
	return personalDetails;
}
public void setPersonalDetails(PersonalDetails personalDetails) {
	this.personalDetails = personalDetails;
}
public ClinicalHistory getClinicalHistory() {
	return clinicalHistory;
}
public void setClinicalHistory(ClinicalHistory clinicalHistory) {
	this.clinicalHistory = clinicalHistory;
}
public MedicalExaminationDetails getMedicalExaminationDetails() {
	return medicalExaminationDetails;
}
public void setMedicalExaminationDetails(
		MedicalExaminationDetails medicalExaminationDetails) {
	this.medicalExaminationDetails = medicalExaminationDetails;
}
public TransportRemuneration getTransportRemuneration() {
	return transportRemuneration;
}
public void setTransportRemuneration(TransportRemuneration transportRemuneration) {
	this.transportRemuneration = transportRemuneration;
}


}
