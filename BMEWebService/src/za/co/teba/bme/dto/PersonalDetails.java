package za.co.teba.bme.dto;

import java.util.Date;

import com.itextpdf.text.Image;


public class PersonalDetails {
	
	private String title;
	private String gender;
	private String surname;
	private String firstName;
	private String birthDate;
	private String idNumber;
	private String passportNumber;
	private String postalAddress;
	private String mineAddress;
	private String homeTelephone;
	private String officeTelephone;
	private String industryNumber;
	private String activeORexmineWorker;
	private String dateLastRiskWork;
	private String occupation;
	private Image signature;
	private Date date;
	private Image photo;
	private String bureauNumber;
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}
	public String getIdNumber() {
		return idNumber;
	}
	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}
	public String getPassportNumber() {
		return passportNumber;
	}
	public void setPassportNumber(String passportNumber) {
		this.passportNumber = passportNumber;
	}
	public String getPostalAddress() {
		return postalAddress;
	}
	public void setPostalAddress(String postalAddress) {
		this.postalAddress = postalAddress;
	}
	public String getMineAddress() {
		return mineAddress;
	}
	public void setMineAddress(String mineAddress) {
		this.mineAddress = mineAddress;
	}
	public String getHomeTelephone() {
		return homeTelephone;
	}
	public void setHomeTelephone(String homeTelephone) {
		this.homeTelephone = homeTelephone;
	}
	public String getOfficeTelephone() {
		return officeTelephone;
	}
	public void setOfficeTelephone(String officeTelephone) {
		this.officeTelephone = officeTelephone;
	}
	public String getIndustryNumber() {
		return industryNumber;
	}
	public void setIndustryNumber(String industryNumber) {
		this.industryNumber = industryNumber;
	}
	public String getActiveORexmineWorker() {
		return activeORexmineWorker;
	}
	public void setActiveORexmineWorker(String activeORexmineWorker) {
		this.activeORexmineWorker = activeORexmineWorker;
	}
	public String getDateLastRiskWork() {
		return dateLastRiskWork;
	}
	public void setDateLastRiskWork(String dateLastRiskWork) {
		this.dateLastRiskWork = dateLastRiskWork;
	}
	public String getOccupation() {
		return occupation;
	}
	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}
	public Image getSignature() {
		return signature;
	}
	public void setSignature(Image signature) {
		this.signature = signature;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Image getPhoto() {
		return photo;
	}
	public void setPhoto(Image photo) {
		this.photo = photo;
	}
	public String getBureauNumber() {
		return bureauNumber;
	}
	public void setBureauNumber(String bureauNumber) {
		this.bureauNumber = bureauNumber;
	}
	
	

}
