package za.co.teba.bme.dto;

import java.util.Date;

import com.itextpdf.text.Image;

public class MedicalExaminationDetails {
	
	private double weight;
	private String urine;
	private double height;
	private String bloodPressure;
	private String generalAppearance;
	private String chestAndLungs;
	private String otherRelevantMedicalInformation;
	private String cxr;
	private String lungFuction;
	private String sputumResults;
	private String other;
	private boolean doYouSmoke;
	private String clinicalSummaryDiagnosis;
	private String anyDisease;
	private Image examinerSignature;
	private String lungFunctionPerformedToday;
	private Date date;
	public double getWeight() {
		return weight;
	}
	public void setWeight(double weight) {
		this.weight = weight;
	}
	public String getUrine() {
		return urine;
	}
	public void setUrine(String urine) {
		this.urine = urine;
	}
	public double getHeight() {
		return height;
	}
	public void setHeight(double height) {
		this.height = height;
	}
	public String getBloodPressure() {
		return bloodPressure;
	}
	public void setBloodPressure(String bloodPressure) {
		this.bloodPressure = bloodPressure;
	}
	public String getGeneralAppearance() {
		return generalAppearance;
	}
	public void setGeneralAppearance(String generalAppearance) {
		this.generalAppearance = generalAppearance;
	}
	public String getChestAndLungs() {
		return chestAndLungs;
	}
	public void setChestAndLungs(String chestAndLungs) {
		this.chestAndLungs = chestAndLungs;
	}
	public String getOtherRelevantMedicalInformation() {
		return otherRelevantMedicalInformation;
	}
	public void setOtherRelevantMedicalInformation(
			String otherRelevantMedicalInformation) {
		this.otherRelevantMedicalInformation = otherRelevantMedicalInformation;
	}
	public String getCxr() {
		return cxr;
	}
	public void setCxr(String cxr) {
		this.cxr = cxr;
	}
	public String getLungFuction() {
		return lungFuction;
	}
	public void setLungFuction(String lungFuction) {
		this.lungFuction = lungFuction;
	}
	public String getSputumResults() {
		return sputumResults;
	}
	public void setSputumResults(String sputumResults) {
		this.sputumResults = sputumResults;
	}
	public String getOther() {
		return other;
	}
	public void setOther(String other) {
		this.other = other;
	}
	public boolean isDoYouSmoke() {
		return doYouSmoke;
	}
	public void setDoYouSmoke(boolean doYouSmoke) {
		this.doYouSmoke = doYouSmoke;
	}
	public String getClinicalSummaryDiagnosis() {
		return clinicalSummaryDiagnosis;
	}
	public void setClinicalSummaryDiagnosis(String clinicalSummaryDiagnosis) {
		this.clinicalSummaryDiagnosis = clinicalSummaryDiagnosis;
	}
	public String getAnyDisease() {
		return anyDisease;
	}
	public void setAnyDisease(String anyDisease) {
		this.anyDisease = anyDisease;
	}
	public Image getExaminerSignature() {
		return examinerSignature;
	}
	public void setExaminerSignature(Image examinerSignature) {
		this.examinerSignature = examinerSignature;
	}
	public String getLungFunctionPerformedToday() {
		return lungFunctionPerformedToday;
	}
	public void setLungFunctionPerformedToday(String lungFunctionPerformedToday) {
		this.lungFunctionPerformedToday = lungFunctionPerformedToday;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	
	
	
}
