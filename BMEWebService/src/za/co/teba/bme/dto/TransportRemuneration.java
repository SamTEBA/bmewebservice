package za.co.teba.bme.dto;

import java.util.Date;

public class TransportRemuneration {
	
	private String liable;
	private Date dateClaimed;
	
	public String getLiable() {
		return liable;
	}
	public void setLiable(String liable) {
		this.liable = liable;
	}
	public Date getDateClaimed() {
		return dateClaimed;
	}
	public void setDateClaimed(Date dateClaimed) {
		this.dateClaimed = dateClaimed;
	}
	

}
