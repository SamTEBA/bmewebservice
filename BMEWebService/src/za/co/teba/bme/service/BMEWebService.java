package za.co.teba.bme.service;

import javax.annotation.Resource;
import javax.jws.WebMethod;
import javax.jws.WebService;

import za.co.teba.bme.entities.BME;
import za.co.teba.bme.entities.Tracking;

@WebService
@Resource(name = "jdbc/TTC", type = javax.sql.DataSource.class, lookup = "jdbc/TTC")
public interface BMEWebService {

	@WebMethod	
	public Long setBME(BME bme);
	
	@WebMethod	
	public Tracking setTracking(Tracking tracking, Long bme);


	@WebMethod	
	public Tracking updateTracking(Tracking tracking, int status);
	
}

/**

@WebMethod(operationName = "AddAbandonmentType")
@WebResult(name = "AbandonmentID")
public Long addAbandonment(@WebParam(name = "AbandonmentType") Abandonment abandonment) {

	em.persist(abandonment);

	return abandonment.getAbandonmentID();
}
*/