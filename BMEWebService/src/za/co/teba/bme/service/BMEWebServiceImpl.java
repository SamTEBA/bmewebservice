package za.co.teba.bme.service;

import java.util.Date;

import javax.annotation.Resource;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.WebResult;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import za.co.teba.bme.entities.BME;
import za.co.teba.bme.entities.Tracking;

@Stateless
@LocalBean
@WebService
@Resource(name = "jdbc/TTC", type = javax.sql.DataSource.class, lookup = "jdbc/TTC")
public class BMEWebServiceImpl implements BMEWebService{
	
	@PersistenceContext(name = "BMEJPA")
	private EntityManager em;

	@Override
	@WebMethod(operationName = "setBME")
	@WebResult(name = "BMEID")
	public Long setBME(BME bme) {
		
		em.persist(bme);
		return bme.getBme_ID();
	}
	
	@Override
	@WebMethod(operationName = "setTracking")
	@WebResult(name = "TrackingID")
	public Tracking setTracking(Tracking tracking ,Long bme) {
		
		tracking.setBME_ID(bme);
		em.persist(tracking);
		return tracking;
	}

	@Override
	@WebMethod(operationName = "updateTracking")
	@WebResult(name = "TrackingID")
	public Tracking updateTracking(Tracking tracking, int status) {
		em.merge(tracking);
		
		Tracking trackingNew = new Tracking();
		trackingNew.setTrack_Start(new Date());
		trackingNew.setBME_ID(tracking.getBME_ID());
		trackingNew.setLast_Touched(new Date());
		trackingNew.setTracking_Status_ID(status);
		em.persist(trackingNew);
		
		return trackingNew;
	}
}
