package za.co.teba.bme.doc;


/*
 * This example was written by Bruno Lowagie in answer to the following question:
 * http://stackoverflow.com/questions/27989754/itext-failure-with-adding-elements-with-5-5-4
 */
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

import za.co.teba.bme.dto.BenefitMedicalExamination;
import za.co.teba.bme.dto.MedicalExamination;
public class BMEDocMapping {
    public static final String SRC = "docs/Z4687847.pdf";
    public static final String DEST = "docs/Out/Z4687847.pdf";
    public static void main(String[] args) throws IOException, DocumentException {
        File file = new File(DEST);
        file.getParentFile().mkdirs();
        BenefitMedicalExamination bme =testData();
        new BMEDocMapping().manipulatePdf(SRC, DEST,bme);
        System.out.println("Done");
    }
    public void manipulatePdf(String src, String dest, BenefitMedicalExamination benefitMedicalExamination) throws IOException, DocumentException {
        PdfReader reader = new PdfReader(src);
        PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(dest));
        PdfContentByte cb = stamper.getOverContent(1);
        ColumnText ct = new ColumnText(cb);
        BaseFont bf = BaseFont.createFont(BaseFont.HELVETICA_BOLD, "Cp1252", BaseFont.EMBEDDED);
        Font f = new Font(bf, 11);
        Paragraph pz;
        
        if(benefitMedicalExamination.getMedicalExamination().getIsThisFirstExamination().equalsIgnoreCase("Y")){
        ct.setSimpleColumn(215f, 48f, 400f, 416f);
        pz= new Paragraph(new Phrase(5, "X", f));
        ct.addElement(pz);
        ct.go();
        }
        else{
        ct.setSimpleColumn(245f, 48f, 400f, 416f);
        pz = new Paragraph(new Phrase(5, "X", f));
        ct.addElement(pz);
        ct.go();
        }
        
        
        ct.setSimpleColumn(570f, 48f, 500f, 415f);
        pz = new Paragraph(new Phrase(5, "2018/05/02", f));
        ct.addElement(pz);
        ct.go();
        ct.setSimpleColumn(570f, 90f, 500f, 397f);
        pz = new Paragraph(new Phrase(5, "CT20180502", f));
        ct.addElement(pz);
        ct.go();
        ct.setSimpleColumn(200f, 48f, 400f, 397f);
        pz = new Paragraph(new Phrase(5, "TEBA LTD", f));
        ct.addElement(pz);
        ct.go();
        ct.setSimpleColumn(200f, 48f, 500f, 380f);
        pz = new Paragraph(new Phrase(5, "2018/06/08", f));
        ct.addElement(pz);
        ct.go();
        ct.setSimpleColumn(496f, 90f, 500f, 380f);
        pz = new Paragraph(new Phrase(5, "X", f));
        ct.addElement(pz);
        ct.go();
        ct.setSimpleColumn(515f, 90f, 525f, 380f);
        pz = new Paragraph(new Phrase(5, "X", f));
        ct.addElement(pz);
        ct.go();
        ct.setSimpleColumn(536f, 48f, 550f, 380f);
        pz = new Paragraph(new Phrase(5, "X", f));
        ct.addElement(pz);
        ct.go();
        ct.setSimpleColumn(556f, 48f, 575f, 380f);
        pz = new Paragraph(new Phrase(5, "X", f));
        ct.addElement(pz);
        ct.go();
        ct.setSimpleColumn(390f, 90f, 500f, 363f);
        pz = new Paragraph(new Phrase(5, "KWA ZULU NATALA", f));
        ct.addElement(pz);
        ct.go();
        ct.setSimpleColumn(215f, 48f, 400f, 310f);
        pz = new Paragraph(new Phrase(5, "X", f));
        ct.addElement(pz);
        ct.go();
        ct.setSimpleColumn(245f, 48f, 400f, 310f);
        pz = new Paragraph(new Phrase(5, "X", f));
        ct.addElement(pz);
        ct.go();
        ct.setSimpleColumn(470f, 90f, 550f, 313f);
        pz = new Paragraph(new Phrase(5, "2018/05/02", f));
        ct.addElement(pz);
        ct.go();
        //// Page 2
        cb = stamper.getOverContent(2);
        ct = new ColumnText(cb);
        ct.setSimpleColumn(150f, 48f, 400f, 782f);
        pz = new Paragraph(new Phrase(5, "Dr. JR LEKOTA", f));
        ct.addElement(pz);
        ct.go();
        ct.setSimpleColumn(150f, 48f, 400f, 732f);
        pz = new Paragraph(new Phrase(5, "1234 Mabelebele Street", f));
        ct.addElement(pz);
        ct.go();
        ct.setSimpleColumn(150f, 48f, 400f, 715f);
        pz = new Paragraph(new Phrase(5, "Kalafong Heights", f));
        ct.addElement(pz);
        ct.go();
        ct.setSimpleColumn(150f, 48f, 400f, 697f);
        pz = new Paragraph(new Phrase(5, "Pretoria West", f));
        ct.addElement(pz);
        ct.go();
        ct.setSimpleColumn(150f, 48f, 400f, 677f);
        pz = new Paragraph(new Phrase(5, "0002", f));
        ct.addElement(pz);
        ct.go();
        ct.setSimpleColumn(150f, 48f, 500f, 608f);
        pz = new Paragraph(new Phrase(15, "All of my immediate family member they caried the same illnesses ", f));
        ct.addElement(pz);
        ct.go();
        ct.setSimpleColumn(150f, 48f, 500f, 430f);
        pz = new Paragraph(
                new Phrase(15, "Tubacolosis and Arthsma, Tubacolosis and Arthsma, Tubacolosis and Arthsma", f));
        ct.addElement(pz);
        ct.go();
        ct.setSimpleColumn(150f, 48f, 500f, 251f);
        pz = new Paragraph(
                new Phrase(15, "Was crabbled for more that 18 years, Was crabbled for more that 18 years", f));
        ct.addElement(pz);
        ct.go();
        //// Page 3
        cb = stamper.getOverContent(3);
        ct = new ColumnText(cb);
        
        ct.setSimpleColumn(90f, 48f, 500f, 796f);
        pz = new Paragraph(new Phrase(15, 96+" KG", f));
        ct.addElement(pz);
        ct.go();
        
        ct.setSimpleColumn(200f, 48f, 500f, 796f);
        pz = new Paragraph(new Phrase(15, "YLD-Ki", f));
        ct.addElement(pz);
        ct.go();
        
        ct.setSimpleColumn(330f, 48f, 500f, 796f);
        pz = new Paragraph(new Phrase(15, 182+" CMC", f));
        ct.addElement(pz);
        ct.go();
        
        ct.setSimpleColumn(480f, 48f, 550f, 796f);
        pz = new Paragraph(new Phrase(15, "110/135", f));
        ct.addElement(pz);
        ct.go();
        
        ct.setSimpleColumn(150f, 48f, 500f, 777f);
        pz = new Paragraph(new Phrase(15, "Very tall, dark and mascullar, Very tall, dark and mascullar", f));
        ct.addElement(pz);
        ct.go();
        
        ct.setSimpleColumn(150f, 48f, 500f, 683f);
        pz = new Paragraph(new Phrase(15, "poor fuctioning of the left lung, bottom up", f));
        ct.addElement(pz);
        ct.go();
        
        ct.setSimpleColumn(150f, 48f, 500f, 583f);
        pz = new Paragraph(new Phrase(15, "other relevent was simmilar for the previous years", f));
        ct.addElement(pz);
        ct.go();
        
        ct.setSimpleColumn(150f, 48f, 500f, 496);
        pz = new Paragraph(new Phrase(15, "- CXR investigation required is available", f));
        ct.addElement(pz);
        ct.go();
        
        ct.setSimpleColumn(150f, 48f, 500f, 480f);
        pz = new Paragraph(new Phrase(15, "investigaion receives for lung function test", f));
        ct.addElement(pz);
        ct.go();
        
        ct.setSimpleColumn(150f, 48f, 500f, 460f);
        pz = new Paragraph(new Phrase(15, "investigation was not completed on sputum results", f));
        ct.addElement(pz);
        ct.go();
        
        ct.setSimpleColumn(150f, 48f, 500f, 440f);
        pz = new Paragraph(new Phrase(15, "no other information was received or conducted", f));
        ct.addElement(pz);
        ct.go();
        
        ct.setSimpleColumn(250f, 48f, 500f, 425f);
        pz = new Paragraph(new Phrase(15, "X", f));
        ct.addElement(pz);
        ct.go();
        
        ct.setSimpleColumn(550f, 48f, 500f, 425f);
        pz = new Paragraph(new Phrase(15, "X", f));
        ct.addElement(pz);
        ct.go();
        
        ct.setSimpleColumn(150f, 48f, 500f, 405f);
        pz = new Paragraph(new Phrase(15, "linical Summary diagnosis, linical Summary diagnosis, linical Summary diagnosis, linical Summary diagnosis", f));
        ct.addElement(pz);
        ct.go();
        
        ct.setSimpleColumn(33f, 48f, 500f, 240f);
        pz = new Paragraph(new Phrase(15, "Any Disease other that occupational desease", f));
        ct.addElement(pz);
        ct.go();
        
        ct.setSimpleColumn(350f, 48f, 500f, 130f);
        pz = new Paragraph(new Phrase(15, "2018/10/24", f));
        ct.addElement(pz);
        ct.go();
        
        ct.setSimpleColumn(300f, 48f, 500f, 70f);
        pz = new Paragraph(new Phrase(15, "X", f));
        ct.addElement(pz);
        ct.go();
        
        ct.setSimpleColumn(550f, 48f, 500f, 70f);
        pz = new Paragraph(new Phrase(15, "X", f));
        ct.addElement(pz);
        ct.go();
        
        
        // BaseFont bf = BaseFont.createFont(BaseFont.HELVETICA_BOLD, "Cp1252",
        // BaseFont.EMBEDDED);
        // f = new Font(bf, 13);
        ct = new ColumnText(cb);
        ct.setSimpleColumn(120f, 48f, 200f, 150);
        pz = new Paragraph("Hello World!", f);
        //ct.addElement(pz);
        //ct.go();
        stamper.close();
        reader.close();
    }
    
    private static BenefitMedicalExamination testData(){
    	BenefitMedicalExamination bme = new BenefitMedicalExamination();
    	
    	
    	
    	MedicalExamination medicalExamination = new MedicalExamination();
    	medicalExamination.setIsThisFirstExamination("Y");
    	medicalExamination.setCentrePreviousExamination("TEBA");
    	medicalExamination.setCentreTodayExamination("KWA ZULU NATALA");
    	medicalExamination.setDatePreviousExamination(new Date());
    	medicalExamination.setDateTodayExamination(new Date());
    	medicalExamination.setPreviousCertification("CT20180514");
    	medicalExamination.setWhereExaminedToday("B");
    	bme.setMedicalExamination(medicalExamination);
    	
    	return bme;
    }
}
