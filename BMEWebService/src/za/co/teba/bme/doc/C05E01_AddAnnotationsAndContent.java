package za.co.teba.bme.doc;

/*
 * This example is part of the iText 7 tutorial.
 */
 
/*import com.itextpdf.forms.PdfAcroForm;
import com.itextpdf.forms.fields.PdfButtonFormField;
import com.itextpdf.forms.fields.PdfFormField;
import com.itextpdf.io.font.FontConstants;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.geom.Rectangle;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfReader;
import com.itextpdf.kernel.pdf.PdfString;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.kernel.pdf.action.PdfAction;
import com.itextpdf.kernel.pdf.annot.PdfAnnotation;
import com.itextpdf.kernel.pdf.annot.PdfTextAnnotation;
import com.itextpdf.kernel.pdf.canvas.PdfCanvas;
import com.itextpdf.test.annotations.WrapToTest;
 */
import java.io.File;
import java.io.IOException;
 
/**
 * Simple adding annotations example.
 */

public class C05E01_AddAnnotationsAndContent {
 
    public static final String SRC = "docs/Z4687847.pdf";
    public static final String DEST = "docs/Out/Z4687847.pdf";
 
    public static void main(String args[]) throws IOException {
        File file = new File(DEST);
        file.getParentFile().mkdirs();
     //   new C05E01_AddAnnotationsAndContent().manipulatePdf(SRC, DEST);
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
}
// 
//    public void manipulatePdf(String src, String dest) throws IOException {
// 
//        //Initialize PDF document
//        PdfDocument pdfDoc = new PdfDocument(new PdfReader(src), new PdfWriter(dest));
// 
//        //Add text annotation
//       /* PdfAnnotation ann = new PdfTextAnnotation(new Rectangle(400, 795, 0, 0))
//                .setTitle(new PdfString("BME FORM"))
//                .setContents("Please, sign the form.")
//                .setOpen(true);
//        pdfDoc.getFirstPage().addAnnotation(ann);*/
//        
//        //Page 1, 
// 
//        PdfCanvas FIRSTEXAMINATION_Y = new PdfCanvas(pdfDoc.getFirstPage());
//        FIRSTEXAMINATION_Y.beginText().setFontAndSize(PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD), 12)
//                .moveText(215, 411)
//                .showText("X")
//                .endText();
//        
//        PdfCanvas FIRSTEXAMINATION_N = new PdfCanvas(pdfDoc.getFirstPage());
//        FIRSTEXAMINATION_N.beginText().setFontAndSize(PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD), 12)
//                .moveText(245, 411)
//                .showText("X")
//                .endText();
//        
//        PdfCanvas DATE_PREVIOU_EXAM = new PdfCanvas(pdfDoc.getFirstPage());
//        DATE_PREVIOU_EXAM.beginText().setFontAndSize(PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD), 10)
//                .moveText(470, 411)
//                .showText("2018/05/02")
//                .endText();
//        
//        
//        PdfCanvas PREVIOUS_CERTIFICATION = new PdfCanvas(pdfDoc.getFirstPage());
//        PREVIOUS_CERTIFICATION.beginText().setFontAndSize(PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD), 10)
//                .moveText(470, 392)
//                .showText("CT20180502")
//                .endText();
//        
//        
//        PdfCanvas CENTRE_OF_PREVIOUS_EXAMINATION = new PdfCanvas(pdfDoc.getFirstPage());
//        CENTRE_OF_PREVIOUS_EXAMINATION.beginText().setFontAndSize(PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD), 10)
//                .moveText(200, 392)
//                .showText("TEBA LTD")
//                .endText();
//        
//        
//        PdfCanvas DATE_OF_TODAYS_EXAMINATION = new PdfCanvas(pdfDoc.getFirstPage());
//        DATE_OF_TODAYS_EXAMINATION.beginText().setFontAndSize(PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD), 10)
//                .moveText(200, 375)
//                .showText("2018/06/08")
//                .endText();
//        
//        
//        
//        PdfCanvas WHERE_EXAMINED_TODAY_M = new PdfCanvas(pdfDoc.getFirstPage());
//        WHERE_EXAMINED_TODAY_M.beginText().setFontAndSize(PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD), 12)
//                .moveText(500, 375)
//                .showText("X")
//                .endText();
//        
//        PdfCanvas WHERE_EXAMINED_TODAY_B = new PdfCanvas(pdfDoc.getFirstPage());
//        WHERE_EXAMINED_TODAY_B.beginText().setFontAndSize(PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD), 12)
//                .moveText(518, 375)
//                .showText("X")
//                .endText();
//        
//        PdfCanvas WHERE_EXAMINED_TODAY_P = new PdfCanvas(pdfDoc.getFirstPage());
//        WHERE_EXAMINED_TODAY_P.beginText().setFontAndSize(PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD), 12)
//                .moveText(536, 375)
//                .showText("X")
//                .endText();
//        
//        PdfCanvas WHERE_EXAMINED_TODAY_C = new PdfCanvas(pdfDoc.getFirstPage());
//        WHERE_EXAMINED_TODAY_C.beginText().setFontAndSize(PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD), 12)
//                .moveText(556, 375)
//                .showText("X")
//                .endText();
//        
//        
//        PdfCanvas CENTRE_OF_TODAYS_EXAMINATION = new PdfCanvas(pdfDoc.getFirstPage());
//        CENTRE_OF_TODAYS_EXAMINATION.beginText().setFontAndSize(PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD), 10)
//                .moveText(390, 358)
//                .showText("KWA ZULU NATALA")
//                .endText();
//        
//        PdfCanvas LIABLE_Y = new PdfCanvas(pdfDoc.getFirstPage());
//        LIABLE_Y.beginText().setFontAndSize(PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD), 12)
//                .moveText(215, 310)
//                .showText("X")
//                .endText();
//        
//        PdfCanvas LIABLE_N = new PdfCanvas(pdfDoc.getFirstPage());
//        LIABLE_N.beginText().setFontAndSize(PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD), 12)
//                .moveText(245, 310)
//                .showText("X")
//                .endText();
//        
//        
//        PdfCanvas DATE_CLAIMED = new PdfCanvas(pdfDoc.getFirstPage());
//        DATE_CLAIMED.beginText().setFontAndSize(PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD), 10)
//                .moveText(470, 310)
//                .showText("2018/05/02")
//                .endText();
//        
//      //----------------------------------------------------------------------------------------------Page 2  
//        
//        PdfCanvas NAME_OF_DOCTOR = new PdfCanvas(pdfDoc.getPage(2));
//        NAME_OF_DOCTOR.beginText().setFontAndSize(PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD), 12)
//                .moveText(150, 777)
//                .showText("Dr. JR LEKOTA")
//                .endText();
//        
//        
//        
//        PdfCanvas ADDRESS_OF_DOCTOR_OR_HOSPITAL_LINE1 = new PdfCanvas(pdfDoc.getPage(2));
//        ADDRESS_OF_DOCTOR_OR_HOSPITAL_LINE1.beginText().setFontAndSize(PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD), 10)
//                .moveText(150, 727)
//                .showText("1234 Mabelebele Street")
//                .endText();
//        
//        PdfCanvas ADDRESS_OF_DOCTOR_OR_HOSPITAL_LINE2 = new PdfCanvas(pdfDoc.getPage(2));
//        ADDRESS_OF_DOCTOR_OR_HOSPITAL_LINE2.beginText().setFontAndSize(PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD), 10)
//                .moveText(150, 710)
//                .showText("Kalafong Heights")
//                .endText();
//        
//        PdfCanvas ADDRESS_OF_DOCTOR_OR_HOSPITAL_LINE3 = new PdfCanvas(pdfDoc.getPage(2));
//        ADDRESS_OF_DOCTOR_OR_HOSPITAL_LINE3.beginText().setFontAndSize(PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD), 10)
//                .moveText(150, 690)
//                .showText("Pretoria West")
//                .endText();
//        
//        PdfCanvas ADDRESS_OF_DOCTOR_OR_HOSPITAL_LINE4 = new PdfCanvas(pdfDoc.getPage(2));
//        ADDRESS_OF_DOCTOR_OR_HOSPITAL_LINE4.beginText().setFontAndSize(PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD), 10)
//                .moveText(150, 672)
//                .showText("0002")
//                .endText();
//        
//        PdfCanvas FAMILY_HISTORY = new PdfCanvas(pdfDoc.getPage(2));
//        FAMILY_HISTORY.beginText().setFontAndSize(PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD), 10)
//                .moveText(150, 600)
//                .showText("All of my immediate family member they caried the same illnesses ")
//                .endText();
//        
//        PdfCanvas PREVIOUS_ILLNESSES = new PdfCanvas(pdfDoc.getPage(2));
//        PREVIOUS_ILLNESSES.beginText().setFontAndSize(PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD), 10)
//                .moveText(150, 419)
//                .showText("Tubacolosis and Arthsma")
//                .endText();
//        
//        PdfCanvas HISTORY_OF_PRESENT_ILLNESSES = new PdfCanvas(pdfDoc.getPage(2));
//        HISTORY_OF_PRESENT_ILLNESSES.beginText().setFontAndSize(PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD), 10)
//                .moveText(150, 241)
//                .showText("Was crabbled for more that 18 years")
//                .endText();
//        
//        
//        ///----------------------------------------------------------------------------------------------------------Page3
//        
//        
//        PdfCanvas WEIGHT = new PdfCanvas(pdfDoc.getPage(3));
//        WEIGHT.beginText().setFontAndSize(PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD), 10)
//                .moveText(90, 783)
//                .showText(96+" KG")
//                .endText();
//        
//        PdfCanvas URINE = new PdfCanvas(pdfDoc.getPage(3));
//        URINE.beginText().setFontAndSize(PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD), 10)
//                .moveText(200, 783)
//                .showText("YLD-Ki")
//                .endText();
//        
//        PdfCanvas HEIGHT = new PdfCanvas(pdfDoc.getPage(3));
//        HEIGHT.beginText().setFontAndSize(PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD), 10)
//                .moveText(330, 783)
//                .showText(182+" CMC")
//                .endText();
//        
//        PdfCanvas BLOOD_PRESSURE = new PdfCanvas(pdfDoc.getPage(3));
//        BLOOD_PRESSURE.beginText().setFontAndSize(PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD), 10)
//                .moveText(480, 783)
//                .showText("110/135")
//                .endText();
//        
//        PdfCanvas GENERAL_APPEARANCE = new PdfCanvas(pdfDoc.getPage(3));
//        GENERAL_APPEARANCE.beginText().setFontAndSize(PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD), 10)
//                .moveText(150, 764)
//                .showText("Very tall, \ndark and mascullar")
//                .endText();
//        
//        
//        PdfCanvas CHEST_AND_LUNGS = new PdfCanvas(pdfDoc.getPage(3));
//        CHEST_AND_LUNGS.beginText().setFontAndSize(PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD), 10)
//                .moveText(150, 670)
//                .showText("poor fuctioning of the left lung, bottom up")
//                .endText();
//        
//        PdfCanvas OTHER_RELEVANT_MEDICAL_INFORMATION = new PdfCanvas(pdfDoc.getPage(3));
//        OTHER_RELEVANT_MEDICAL_INFORMATION.beginText().setFontAndSize(PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD), 10)
//                .moveText(150, 570)
//                .showText("other relevent was simmilar for the previous years")
//                .endText();
//        
//        
//        PdfCanvas INVESTIGATION_REQUIRED_CXR = new PdfCanvas(pdfDoc.getPage(3));
//        INVESTIGATION_REQUIRED_CXR.beginText().setFontAndSize(PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD), 10)
//                .moveText(150, 486)
//                .showText("- CXR investigation required is available")
//                .endText();
//        
//        PdfCanvas INVESTIGATION_REQUIRED_LFT = new PdfCanvas(pdfDoc.getPage(3));
//        INVESTIGATION_REQUIRED_LFT.beginText().setFontAndSize(PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD), 10)
//                .moveText(150, 470)
//                .showText("investigaion receives for lung function test")
//                .endText();
//        
//        PdfCanvas INVESTIGATION_REQUIRED_SR = new PdfCanvas(pdfDoc.getPage(3));
//        INVESTIGATION_REQUIRED_SR.beginText().setFontAndSize(PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD), 10)
//                .moveText(150, 450)
//                .showText("investigation was not completed on sputum results")
//                .endText();
//        
//        PdfCanvas INVESTIGATION_REQUIRED_Other = new PdfCanvas(pdfDoc.getPage(3));
//        INVESTIGATION_REQUIRED_Other.beginText().setFontAndSize(PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD), 10)
//                .moveText(150, 430)
//                .showText("no other information was received or conducted")
//                .endText();
//        
//        PdfCanvas DO_YOU_SMOKE_YES = new PdfCanvas(pdfDoc.getPage(3));
//        DO_YOU_SMOKE_YES.beginText().setFontAndSize(PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD), 12)
//                .moveText(250, 410)
//                .showText("X")
//                .endText();
//        PdfCanvas DO_YOU_SMOKE_No = new PdfCanvas(pdfDoc.getPage(3));
//        DO_YOU_SMOKE_No.beginText().setFontAndSize(PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD), 12)
//                .moveText(550, 410)
//                .showText("X")
//                .endText();
//        
//        PdfCanvas CLINICAL_SUMMARY_DIAGNOSIS = new PdfCanvas(pdfDoc.getPage(3));
//        CLINICAL_SUMMARY_DIAGNOSIS.beginText().setFontAndSize(PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD), 10)
//                .moveText(150, 395)
//                .showText("Clinical Summary diagnosis")
//                .endText();
//        
//        PdfCanvas ANY_DISEASE_OTHER_THAN_OCCUPATIONAL_DISEASE = new PdfCanvas(pdfDoc.getPage(3));
//        ANY_DISEASE_OTHER_THAN_OCCUPATIONAL_DISEASE.beginText().setFontAndSize(PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD), 10)
//                .moveText(33, 230)
//                .showText("Any Disease other that occupational desease")
//                .endText();
//        
//        PdfCanvas DATE_DOCTOR = new PdfCanvas(pdfDoc.getPage(3));
//        DATE_DOCTOR.beginText().setFontAndSize(PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD), 10)
//                .moveText(350, 120)
//                .showText("2018/10/24")
//                .endText();
//        
//        PdfCanvas LUNG_FUNCTION_TESTS_PERFORMED_TODAY_YES = new PdfCanvas(pdfDoc.getPage(3));
//        LUNG_FUNCTION_TESTS_PERFORMED_TODAY_YES.beginText().setFontAndSize(PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD), 12)
//                .moveText(300, 60)
//                .showText("X")
//                .endText();
//        
//        PdfCanvas LUNG_FUNCTION_TESTS_PERFORMED_TODAY_NO = new PdfCanvas(pdfDoc.getPage(3));
//        LUNG_FUNCTION_TESTS_PERFORMED_TODAY_NO.beginText().setFontAndSize(PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD), 12)
//                .moveText(550, 60)
//                .showText("X")
//                .endText();
//      
//        pdfDoc.close();
// 
//    }
//}