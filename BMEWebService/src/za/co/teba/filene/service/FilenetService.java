package za.co.teba.filene.service;

import java.io.File;

import javax.jws.WebService;
import javax.jws.WebMethod;

import za.co.teba.filene.data.RequestInfoBO;
import za.co.teba.filene.data.RespondBO;



@WebService
public interface FilenetService {

@WebMethod	
public RespondBO addDocument(RequestInfoBO requestInfoBO);


@WebMethod	
public RespondBO updateDocuments(String id, int match);


@WebMethod	
public File getImagePhoto(String id);
}