package za.co.teba.filene.data;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAccessType;

@XmlRootElement(name="RequestInfoBO")
@XmlAccessorType(XmlAccessType.FIELD)
public class RequestInfoBO extends RespondBO{

	 private String  projectName;
	 private String industryNumber;
	 private List<String> documentList;
	 
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public String getIndustryNumber() {
		return industryNumber;
	}
	public void setIndustryNumber(String industryNumber) {
		this.industryNumber = industryNumber;
	}
	public List<String> getDocumentList() {
		return documentList;
	}
	public void setDocumentList(List<String> documentList) {
		this.documentList = documentList;
	}
	@Override
	public String toString() {
		return "RequestInfoBO [projectName=" + projectName
				+ ", industryNumber=" + industryNumber + ", documentList="
				+ documentList + "]";
	}

}
