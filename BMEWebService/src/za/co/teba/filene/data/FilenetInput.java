package za.co.teba.filene.data;

import java.util.List;

public class FilenetInput {
	
	private String projectName;
	private List<String> fileNames;
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public List<String> getFileNames() {
		return fileNames;
	}
	public void setFileNames(List<String> fileNames) {
		this.fileNames = fileNames;
	}

}
