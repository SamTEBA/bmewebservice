package za.co.teba.filene.util;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import za.co.teba.filene.data.ErrorBO;
import za.co.teba.filene.data.RequestInfoBO;



/**
 * 
 * @author samr
 * 
 */
public class Helper {

	private static Helper instance;

	private Helper() {
	}

	public static Helper getInstance() {
		if (instance == null) {
			instance = new Helper();
		}
		return instance;
	}

	/**
	 * 
	 * @param rs
	 * @return
	 * @throws SQLException
	 */
	public RequestInfoBO createRequestInfoBO(ResultSet rs)
			throws SQLException {
		RequestInfoBO requestInfo = null;

		

		return requestInfo;

	}

	/**
	 * 
	 * @param error
	 * @return
	 */
	public ErrorBO createIdErrorObject(ErrorBO error) {
		error.setErrorType("Exception");
		error.setErrorName("ID Not Valid");
		error.setDescription("The ID number provided is not valid");

		return error;
	}

	/**
	 * 
	 * @param error
	 * @return
	 */
	public ErrorBO createNoInfoErrorObject(ErrorBO error) {
		error.setErrorType("Exception");
		error.setErrorName("No Info Found");
		error.setDescription("The ID number provided has no info in our record database");

		return error;
	}

	/**
	 * 
	 * @param error
	 * @return
	 */
	public ErrorBO createSystemErrorObject(ErrorBO error) {
		error.setErrorType("Exception");
		error.setErrorName("System Error");
		error.setDescription("Please contact administrator");

		return error;
	}

}
