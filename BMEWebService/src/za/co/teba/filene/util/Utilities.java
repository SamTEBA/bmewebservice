package za.co.teba.filene.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 
 * @author samr
 * 
 */
public class Utilities {

	private static Utilities instance;

	private Utilities() {
	}

	public static Utilities getInstance() {
		if (instance == null) {
			instance = new Utilities();
		}
		return instance;
	}

	/**
	 * 
	 * @param date
	 * @return
	 */
	public Date StringToDate(String date) {
		DateFormat df = new SimpleDateFormat("yyyy/MM/dd");
		Date startDate = null;
		try {
			startDate = df.parse(date);
			String newDateString = df.format(startDate);
			System.out.println(newDateString);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return startDate;
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public boolean validateID(String id) {
		if (id.length() != 13 && id.length() != 8) {
			System.out.println("ID number NOT 13 NOR 8 long ... is " + id.length());
			return false;
		}
		
		if (id.length() == 13){
		    return isNumericRegx(id);
		}
		return true;
	}

	/**
	 * 
	 * @param str
	 * @return
	 */
	public boolean isNumericRegx(String str) {
		return str.matches("-?\\d+(\\.\\d+)?"); // match a number with optional
												// '-' and decimal.
	}

	/**
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isNumeric(String str) {
		try {
			@SuppressWarnings("unused")
			double d = Double.parseDouble(str);
		} catch (NumberFormatException nfe) {
			return false;
		}
		return true;
	}

	public static void main(String[] args) {
		System.out.println(Utilities.getInstance().validateID("840628533608a"));
	}
}
