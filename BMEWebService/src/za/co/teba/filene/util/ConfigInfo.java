package za.co.teba.filene.util;

public final class ConfigInfo {
	
	/**
     * The sample shows the use of a JAAS login and also the use of the
     * API helpers for callers who still use the userid/password model.
     * Which is which is controlled by this variable.  In either case, you
     * will need to have configured JAAS for your JVM.
     */
	public static boolean USE_EXPLICIT_JAAS_LOGIN = true;
    /**
     * If you use the API helpers, you need a userid and password.  To keep
     * the sample simple, we've assumed the explicit JAAS login also needs
     * them (via callbacks).
     */
	public static String USERID = "fnadmin";
    /**
     * See comments for USERID.
     */
	public static String PASSWORD = "Gr3yScr33n";
    /**
     * More JAAS configuration.  Which stanza within our JAAS configuration
     * should you use?
     */
	public static String JAAS_STANZA_NAME = "FileNetP8WSI";	
	
    /**
     * This URI tells us how to find the CE and what protocol to use.
     */
    public static String CE_URI = "http://tdtconfoun01.teba.co.za:9080/wsi/FNCEWS40MTOM";
    /** 
     * This ObjectStore must already exist.  It's where all the
     * activity will happen.
     */
    public static String OBJECT_STORE_NAME = "DevObj01";
    /**
     * Documents will be filed into this folder.  If it doesn't exist, it
     * will be created (directly under the root folder).  Don't include
     * any slashes in this folder name.
     */
    public static String FOLDER_BME_ROOT = "BMEDocuments";
    /**
     * This is the local file from which we will take content for the
     * newly created repository document.  There aren't any particular
     * constraints on size or type.
     */
    public static String LOCAL_FILE_NAME = "c:/temp/SomeFile.txt";
    /**
     * Completely unnecessary for the system, but people tend to like
     * seeing this populated.
     */
    public static String DOCUMENT_TITLE = "My Document Title";
    /**
     * We set this explicitly to illustrate that it's unrelated to the
     * DocumentTitle as well as the original file name.  When you are
     * referencing a document by path, you use the containment name.
     */
    public static String CONTAINMENT_NAME = "I Am a Contained Document";
    /**
     * You get your choice on the readback.  You can either read and
     * compare from the beginning or you can skip to a quasi-random point
     * in the second half of the content and start there.
     */
    public static boolean USE_SKIP = true;
    
    

}
